# Ejercicio 05 - Red Extract, Binarizar y Filtrar
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
# Daniel Velazquez Oct 18

#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
import numpy as np
import cv2
import math
from statistics import median

def get_filtered_frame(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    lower_red = np.array([0,50,50])
    upper_red = np.array([10,255,255])
    mask = cv2.inRange(image, lower_red, upper_red)
    filtered_image = cv2.bitwise_and(image, image, mask=mask)
    filtered_image = cv2.cvtColor(filtered_image, cv2.COLOR_BGR2GRAY)
    retval, filtered_image = cv2.threshold(filtered_image, 120, 255, cv2.THRESH_BINARY)
    return filtered_image

def draw_lines(image, lines, color=[255, 0, 0], thickness=3):
    # If there are no lines to draw, exit.
    if lines is None:
            return
    # Make a copy of the original image.
    image = np.copy(image)
    # Create a blank image that matches the original in size.
    line_img = np.zeros(
        (
            image.shape[0],
            image.shape[1],
            3
        ),
        dtype=np.uint8,
    )
    # Loop over all lines and draw them on the blank image.
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(line_img, (x1, y1), (x2, y2), color, thickness)
    # Merge the image with the lines onto the original.
    image = cv2.addWeighted(image, 0.8, line_img, 1.0, 0.0)
    # Return the modified image.
    return image

def get_image_houghlines(image):
    lines = cv2.HoughLinesP(
        image,
        rho=6,
        theta=np.pi / 60,
        threshold=160,
        lines=np.array([]),
        minLineLength=40,
        maxLineGap=25
    )
    return lines

def get_guidance_lines(lines, image):
    left_line_x = []
    left_line_y = []
    right_line_x = []
    right_line_y = []
    for line in lines:
        for x1, y1, x2, y2 in line:
            slope = (y2 - y1) / (x2 - x1) # <-- Calculating the slope.
            if math.fabs(slope) < 0.5: # <-- Only consider extreme slope
                continue
            if slope <= 0: # <-- If the slope is negative, left group.
                left_line_x.extend([x1, x2])
                left_line_y.extend([y1, y2])
            else: # <-- Otherwise, right group.
                right_line_x.extend([x1, x2])
                right_line_y.extend([y1, y2])
    min_y = image.shape[0]*(0)  # <-- Just below the horizon
    max_y = image.shape[0] # <-- The bottom of the image
    poly_left = np.poly1d(np.polyfit(
        left_line_y,
        left_line_x,
        deg=1
    ))
    left_x_start = int(poly_left(max_y))
    left_x_end = int(poly_left(min_y))
    poly_right = np.poly1d(np.polyfit(
        right_line_y,
        right_line_x,
        deg=1
    ))

    right_x_start = int(poly_right(max_y))
    right_x_end = int(poly_right(min_y))
    linesxy={'min_y':min_y,'max_y':max_y,'left_x_start':left_x_start,'left_x_end':left_x_end,'right_x_start':right_x_start,'right_x_end':right_x_end}
    return linesxy

def place_lines_in_image(image, linesxy):
    line_image = draw_lines(
        image,
        [[
            [int(linesxy['left_x_start']), int(linesxy['max_y']), int(linesxy['left_x_end']), int(linesxy['min_y'])],
            [int(linesxy['right_x_start']), int(linesxy['max_y']), int(linesxy['right_x_end']), int(linesxy['min_y'])],
        ]],
        thickness=5,
    )
    return line_image

# <<<<<<< HEAD
def clean_guideline_list(guideline_list):
    left_x_end = []
    left_x_start = []
    right_x_end = []
    right_x_start = []
    for guideline in guideline_list:
        left_x_end.append(guideline["left_x_end"])
        left_x_start.append(guideline["left_x_start"])
        right_x_end.append(guideline["right_x_end"])
        right_x_start.append(guideline["right_x_start"])
        min_y = guideline["min_y"]
        max_y = guideline["max_y"]
    cleansed_guidelines = {
        "min_y": min_y,
        "max_y": max_y,
        "left_x_end": median(left_x_end),
        "left_x_start": median(left_x_start),
        "right_x_end": median(right_x_end),
        "right_x_start": median(right_x_start)
    }
    return cleansed_guidelines

# =============== PROGRAM START ================================================
cap = cv2.VideoCapture(0)
guideline_list = []
i = 0
# =======
# def retain_line (lines, cucu):
#     linesxy = get_guidance_lines(lines, cucu)
#     if not bool(linesxy):
#         print("Retain last value")
#         linesxy=linesxxyy
#         counter+=1
#     else:
#         linesxxyy=linesxy
#         counter=0
#     if counter > 8:
#         print("No hay nada de lineas")
#     return linesxy
# # =============== PROGRAM START ================================================
# cap = cv2.VideoCapture(0)
# linesxxyy=[]
# counter=0
# >>>>>>> cbd22de08c940a29e3992145ed207fd89cd273de
while (True):
    ret, cucu = cap.read()
    filtered_image = get_filtered_frame(cucu)
    cannyed_image = cv2.Canny(filtered_image, 100, 200)
    try:
        lines = get_image_houghlines(cannyed_image)
        linesxy = get_guidance_lines(lines, cucu)

        guideline_list.append(linesxy)
        if i == 5:
            cleansed_guidelines = clean_guideline_list(guideline_list)
            i = 0
            guideline_list = []
        else:
            i += 1
        #If not the fifth iteration then printable line remains the same
        line_image = place_lines_in_image(cucu, cleansed_guidelines)
        cv2.imshow("camara", line_image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
        	break
    except Exception as e:
        print(e)
        cv2.imshow("camara", cucu)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
