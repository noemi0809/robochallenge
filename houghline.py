# Ejercicio 05 - Red Extract, Binarizar y Filtrar
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
# Daniel Velazquez Oct 18

#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
import numpy as np
import cv2
import math

def draw_lines(img, lines, color=[255, 0, 0], thickness=3):
    # If there are no lines to draw, exit.
    if lines is None:
            return
    # Make a copy of the original image.
    img = np.copy(img)
    # Create a blank image that matches the original in size.
    line_img = np.zeros(
        (
            img.shape[0],
            img.shape[1],
            3
        ),
        dtype=np.uint8,
    )
    # Loop over all lines and draw them on the blank image.
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(line_img, (x1, y1), (x2, y2), color, thickness)
    # Merge the image with the lines onto the original.
    img = cv2.addWeighted(img, 0.8, line_img, 1.0, 0.0)
    # Return the modified image.
    return img

cap = cv2.VideoCapture(0)

#cv2.namedWindow("Camera", cv2.WND_PROP_FULLSCREEN);
#cv2.namedWindow("Camera", 1);
#cv2.setWindowProperty("Camera", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN);
#cv2.setWindowProperty("Camera", 1, 3);


while (True):
	ret, frame = cap.read()
	cucu=frame
	frame=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	lower_red = np.array([0,50,50]) 	#example value
	upper_red = np.array([10,255,255]) 	#example value
	mask = cv2.inRange(frame, lower_red, upper_red)
	img_result = cv2.bitwise_and(frame, frame, mask=mask)

	img_result=cv2.cvtColor(img_result, cv2.COLOR_BGR2GRAY)

	# THRESHOLDING
	retval, img_result = cv2.threshold(img_result,120,255,cv2.THRESH_BINARY)

	# AQUI ES EL FILTRADO
	median = cv2.medianBlur(img_result,5)


	# Convert to grayscale here.
	#gray_image = cv2.cvtColor(median, cv2.COLOR_RGB2GRAY)
	# Call Canny Edge Detection here.
	cannyed_image = cv2.Canny(median, 100, 200)
	#plt.figure()
	#plt.imshow(cannyed_image)
	#plt.show()
	lines = cv2.HoughLinesP(
    cannyed_image,
    rho=6,
    theta=np.pi / 60,
    threshold=160,
    lines=np.array([]),
    minLineLength=40,
    maxLineGap=25
	)
	print(lines)

#right and left draw_lines
	left_line_x = []
	left_line_y = []
	right_line_x = []
	right_line_y = []
	for line in lines:
	    for x1, y1, x2, y2 in line:
	        slope = (y2 - y1) / (x2 - x1) # <-- Calculating the slope.
	        if math.fabs(slope) < 0.5: # <-- Only consider extreme slope
	            continue
	        if slope <= 0: # <-- If the slope is negative, left group.
	            left_line_x.extend([x1, x2])
	            left_line_y.extend([y1, y2])
	        else: # <-- Otherwise, right group.
	            right_line_x.extend([x1, x2])
	            right_line_y.extend([y1, y2])
	min_y = cucu.shape[0]*(0)  # <-- Just below the horizon
	max_y = cucu.shape[0] # <-- The bottom of the image
	try:
		poly_left = np.poly1d(np.polyfit(
	    		left_line_y,
	    		left_line_x,
	    		deg=1
		))
	except Exception as e:
		print(e)
	left_x_start = int(poly_left(max_y))
	left_x_end = int(poly_left(min_y))
	try:
		poly_right = np.poly1d(np.polyfit(
	    		right_line_y,
	    		right_line_x,
	    		deg=1
		))
	except Exception as e:
		print(e)
	right_x_start = int(poly_right(max_y))
	right_x_end = int(poly_right(min_y))
	line_image = draw_lines(
	    cucu,
	    [[
	        [int(left_x_start), int(max_y), int(left_x_end), int(min_y)],
	        [int(right_x_start), int(max_y), int(right_x_end), int(min_y)],
	    ]],
	    thickness=5,
	)

#endf



	cv2.imshow("camara", line_image)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
